# Apollo_Cyber_CmakeBuild

#### 介绍
百度Apollo自动驾驶平台cyber模块的CMAKE交叉编译仓库。
用来对cyber模块进行调试、学习或者移植到嵌入式设备中运行。
本项目会对原cyber进行目录结构的调整以及相关的测试代码、例子的执行编译。
Apollo原仓库见：
https://gitee.com/ApolloAuto/apollo

#### 软件架构
NA


#### 安装教程
前置安装软件参考[dep](dep.md)，其中部分依赖已经编译包含在本仓库中([dependency目录](./dependency/))


bash make_build_time.sh 使用cmake&&makefile进行编译

bash ninja_build_time.sh 使用cmake&&ninja进行编译




#### 使用说明

编译完成后执行如下脚本运行cyber中的common_component_example例程
start_cyber_mainboard.sh

start_cyber_example_prediction_writer.sh

start_cyber_example_test_writer.sh

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
