


function(proto_gen proto_module src_path dst_cc_path dst_h_path)
#try run with proc 
execute_process(COMMAND ${PROJECT_SOURCE_DIR}/Tools/Protoc/protoc --version RESULT_VARIABLE RES)
if(${RES} STREQUAL  "0")
else()
    message("Protoc Tool Need Library")
    set( ENV{LD_LIBRARY_PATH} ${PROJECT_SOURCE_DIR}/Tools/Protoc/)
endif()

execute_process(COMMAND rm -f ${dst_cc_path}/*.pb.cc RESULT_VARIABLE RES)
if(${RES} STREQUAL  "0")
else()
    message("Proto Gen Code Delete Code Fail,cmake exit")
    message(FATAL_ERROR "Error")
endif()

execute_process(COMMAND cd ${PROJECT_SOURCE_DIR})
message(${src_path})
execute_process(COMMAND ${PROJECT_SOURCE_DIR}/Tools/Protoc/protoc --proto_path=${src_path} --cpp_out=${dst_cc_path}
${proto_module}.proto RESULT_VARIABLE RES)


if(${RES} STREQUAL  "0")
    message("Proto Gen Code Success")
    execute_process(COMMAND rm -f ${dst_h_path}/*.pb.h RESULT_VARIABLE RES)
    if(${RES} STREQUAL  "0")
    else()
        message("Proto Gen Code Delete Header Fail,cmake exit")
        message(FATAL_ERROR "Error")
    endif()
    #execute_process(COMMAND mv -f ${dst_cc_path}/${proto_module}.pb.h ${dst_h_path} RESULT_VARIABLE RES)
    if(${RES} STREQUAL  "0")
        message("Proto Gen Move Header Success")
    else()
        message("Proto Gen Code Move Header Fail,cmake exit")
        message(FATAL_ERROR "Error")
    endif()
else()
    message("Proto Gen Code Code Fail,cmake exit")
    message(FATAL_ERROR "Error")
endif()
endfunction(proto_gen)