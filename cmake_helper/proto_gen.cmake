
function(proto_gen_wrap module)

proto_gen(${module} ${PROJECT_SOURCE_DIR}/cyber/proto/ ${PROJECT_SOURCE_DIR}/cyber/proto/ ${PROJECT_SOURCE_DIR}/cyber/proto/)
endfunction()

function(call_example_proto_gen)
proto_gen(examples ${PROJECT_SOURCE_DIR}/examples/proto/ ${PROJECT_SOURCE_DIR}/examples/proto/ 
{PROJECT_SOURCE_DIR}/examples/proto/)
endfunction()



function(call_proto_gen)

call_example_proto_gen()


proto_gen_wrap(choreography_conf)
proto_gen_wrap(classic_conf)
proto_gen_wrap(clock)
proto_gen_wrap(component_conf)
proto_gen_wrap(cyber_conf)
proto_gen_wrap(dag_conf)
proto_gen_wrap(parameter)
proto_gen_wrap(perf_conf)
proto_gen_wrap(proto_desc)
proto_gen_wrap(qos_profile)
proto_gen_wrap(record)
proto_gen_wrap(role_attributes)
proto_gen_wrap(run_mode_conf)
proto_gen_wrap(scheduler_conf)
proto_gen_wrap(topology_change)
proto_gen_wrap(transport_conf)
proto_gen_wrap(unit_test)


endfunction()

