/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "cyber/service_discovery/specific_manager/manager.h"

#include "cyber/common/global_data.h"
#include "cyber/common/log.h"
#include "cyber/message/message_traits.h"
#include "cyber/time/time.h"
#include "cyber/transport/qos/qos_profile_conf.h"
#include "cyber/transport/rtps/attributes_filler.h"
#include "cyber/transport/rtps/underlay_message.h"
#include "cyber/transport/rtps/underlay_message_type.h"
#include "cyber/dbg/hexdump.h"


namespace apollo {
namespace cyber {
namespace service_discovery {

using transport::AttributesFiller;
using transport::QosProfileConf;

Manager::Manager()
    : is_shutdown_(false),
      is_discovery_started_(false),
      allowed_role_(0),
      change_type_(proto::ChangeType::CHANGE_PARTICIPANT),
      channel_name_(""),
      publisher_(nullptr),
      subscriber_(nullptr),
      listener_(nullptr) {
  host_name_ = common::GlobalData::Instance()->HostName();
  process_id_ = common::GlobalData::Instance()->ProcessId();
}

Manager::~Manager() { Shutdown(); }

bool Manager::StartDiscovery(RtpsParticipant* participant) {
  if (participant == nullptr) {
    return false;
  }
  if (is_discovery_started_.exchange(true)) {
    return true;
  }
  if (!CreatePublisher(participant) || !CreateSubscriber(participant)) {
    AERROR << "create publisher or subscriber failed.";
    StopDiscovery();
    return false;
  }
  return true;
}

void Manager::StopDiscovery() {
  if (!is_discovery_started_.exchange(false)) {
    return;
  }

  {
    std::lock_guard<std::mutex> lg(lock_);
    if (publisher_ != nullptr) {
      eprosima::fastrtps::Domain::removePublisher(publisher_);
      publisher_ = nullptr;
    }
  }

  if (subscriber_ != nullptr) {
    eprosima::fastrtps::Domain::removeSubscriber(subscriber_);
    subscriber_ = nullptr;
  }

  if (listener_ != nullptr) {
    delete listener_;
    listener_ = nullptr;
  }
}

void Manager::Shutdown() {
  if (is_shutdown_.exchange(true)) {
    return;
  }

  StopDiscovery();
  signal_.DisconnectAllSlots();
}

bool Manager::Join(const RoleAttributes& attr, RoleType role,
                   bool need_publish) {
#ifdef CYBER_DBG
  AINFO << "Manager::Join Process id:"<< attr.process_id() <<
  "\n Node Name:"<<attr.node_name()<<
  "\n Channel Name:"<<attr.channel_name()<<
  "\n Message Type:"<<attr.message_type()<<
  "\n RoleType:"<<role;
#endif

  if (is_shutdown_.load()) {
    ADEBUG << "the manager has been shut down.";
    return false;
  }
  RETURN_VAL_IF(!((1 << role) & allowed_role_), false);
  RETURN_VAL_IF(!Check(attr), false);
  ChangeMsg msg;
  Convert(attr, role, OperateType::OPT_JOIN, &msg);
  Dispose(msg);
  if (need_publish) {
    return Publish(msg);
  }
  return true;
}

bool Manager::Leave(const RoleAttributes& attr, RoleType role) {
  if (is_shutdown_.load()) {
    ADEBUG << "the manager has been shut down.";
    return false;
  }
  RETURN_VAL_IF(!((1 << role) & allowed_role_), false);
  RETURN_VAL_IF(!Check(attr), false);
  ChangeMsg msg;
  Convert(attr, role, OperateType::OPT_LEAVE, &msg);
  Dispose(msg);
  if (NeedPublish(msg)) {
    return Publish(msg);
  }
  return true;
}

Manager::ChangeConnection Manager::AddChangeListener(const ChangeFunc& func) {
  return signal_.Connect(func);
}

void Manager::RemoveChangeListener(const ChangeConnection& conn) {
  auto local_conn = conn;
  local_conn.Disconnect();
}

bool Manager::CreatePublisher(RtpsParticipant* participant) {
  RtpsPublisherAttr pub_attr;
  RETURN_VAL_IF(
      !AttributesFiller::FillInPubAttr(
          channel_name_, QosProfileConf::QOS_PROFILE_TOPO_CHANGE, &pub_attr),
      false);
#ifdef CYBER_DBG
  AINFO << "Manager::CreatePublisher Topic Name:"<<pub_attr.topic.getTopicName();
#endif
  publisher_ =
      eprosima::fastrtps::Domain::createPublisher(participant, pub_attr);
  return publisher_ != nullptr;
}

bool Manager::CreateSubscriber(RtpsParticipant* participant) {
  RtpsSubscriberAttr sub_attr;
  RETURN_VAL_IF(
      !AttributesFiller::FillInSubAttr(
          channel_name_, QosProfileConf::QOS_PROFILE_TOPO_CHANGE, &sub_attr),
      false);
  listener_ = new SubscriberListener(
      std::bind(&Manager::OnRemoteChange, this, std::placeholders::_1));

  subscriber_ = eprosima::fastrtps::Domain::createSubscriber(
      participant, sub_attr, listener_);
  return subscriber_ != nullptr;
}

bool Manager::NeedPublish(const ChangeMsg& msg) const {
  (void)msg;
  return true;
}


void Manager::RtpsMsgConvert(const apollo::cyber::transport::UnderlayMessage& rtpsMsg,ChangeMsg* msg)
{
  using namespace apollo::cyber::transport;

  auto rtMsg = rtpsMsg.changemsg();

  msg->set_timestamp(rtMsg.timestamp());
  msg->set_change_type(static_cast<ChangeType>(rtMsg.change_type()));
  msg->set_operate_type(static_cast<OperateType>(rtMsg.operate_type() ));
  msg->set_role_type(static_cast<RoleType>(rtMsg.role_type()));
  msg->mutable_role_attr()->set_host_name(rtMsg.role_attr().host_name());
  msg->mutable_role_attr()->set_host_ip(rtMsg.role_attr().host_ip());
  msg->mutable_role_attr()->set_process_id(rtMsg.role_attr().process_id());
  msg->mutable_role_attr()->set_node_name(rtMsg.role_attr().node_name());
  msg->mutable_role_attr()->set_node_id(rtMsg.role_attr().node_id());
  msg->mutable_role_attr()->set_channel_name(rtMsg.role_attr().channel_name());
  msg->mutable_role_attr()->set_channel_id(rtMsg.role_attr().channel_id());
  msg->mutable_role_attr()->set_message_type(rtMsg.role_attr().message_type());
  msg->mutable_role_attr()->set_proto_desc(rtMsg.role_attr().proto_desc());
  msg->mutable_role_attr()->set_id(rtMsg.role_attr().id());
  msg->mutable_role_attr()->mutable_qos_profile()->set_history(static_cast<QosHistoryPolicy>(rtMsg.role_attr().qos_profile().history()));
  msg->mutable_role_attr()->mutable_qos_profile()->set_depth(rtMsg.role_attr().qos_profile().depth());
  msg->mutable_role_attr()->mutable_qos_profile()->set_mps(rtMsg.role_attr().qos_profile().mps());
  msg->mutable_role_attr()->mutable_qos_profile()->set_reliability(static_cast<QosReliabilityPolicy>(rtMsg.role_attr().qos_profile().reliability()));
  msg->mutable_role_attr()->mutable_qos_profile()->set_durability(static_cast<QosDurabilityPolicy>(rtMsg.role_attr().qos_profile().durability()));
  msg->mutable_role_attr()->mutable_socket_addr()->set_ip(rtMsg.role_attr().socket_addr().ip());
  msg->mutable_role_attr()->mutable_socket_addr()->set_port(rtMsg.role_attr().socket_addr().port());
  msg->mutable_role_attr()->set_service_name(rtMsg.role_attr().service_name());
  msg->mutable_role_attr()->set_service_id(rtMsg.role_attr().service_id());

}


void Manager::ProtoMsgConvert(const ChangeMsg& msg,apollo::cyber::transport::UnderlayMessage* rtpsMsg)
{
  using namespace apollo::cyber::transport;

  rtpsMsg->changemsg().timestamp() = msg.timestamp();
  rtpsMsg->changemsg().change_type() = static_cast<ChangeType_IDL>(msg.change_type());
  rtpsMsg->changemsg().operate_type() = static_cast<OperateType_IDL>(msg.operate_type());
  rtpsMsg->changemsg().role_type() = static_cast<RoleType_IDL>(msg.role_type());
  rtpsMsg->changemsg().role_attr().host_name() = msg.role_attr().host_name();
  rtpsMsg->changemsg().role_attr().host_ip() = msg.role_attr().host_ip();
  rtpsMsg->changemsg().role_attr().process_id() = msg.role_attr().process_id();
  rtpsMsg->changemsg().role_attr().node_name() = msg.role_attr().node_name();
  rtpsMsg->changemsg().role_attr().node_id() = msg.role_attr().node_id();
  rtpsMsg->changemsg().role_attr().channel_name() = msg.role_attr().channel_name();
  rtpsMsg->changemsg().role_attr().channel_id() = msg.role_attr().channel_id();
  rtpsMsg->changemsg().role_attr().message_type() = msg.role_attr().message_type();
  rtpsMsg->changemsg().role_attr().proto_desc() = msg.role_attr().proto_desc();
  rtpsMsg->changemsg().role_attr().id() = msg.role_attr().id();
  rtpsMsg->changemsg().role_attr().qos_profile().history() = static_cast<QosHistoryPolicy_IDL>(msg.role_attr().qos_profile().history());
  rtpsMsg->changemsg().role_attr().qos_profile().depth() = msg.role_attr().qos_profile().depth();
  rtpsMsg->changemsg().role_attr().qos_profile().mps() = msg.role_attr().qos_profile().mps();
  rtpsMsg->changemsg().role_attr().qos_profile().reliability() = static_cast<QosReliabilityPolicy_IDL>(msg.role_attr().qos_profile().reliability());
  rtpsMsg->changemsg().role_attr().qos_profile().durability() = static_cast<QosDurabilityPolicy_IDL>(msg.role_attr().qos_profile().durability());
  
  rtpsMsg->changemsg().role_attr().socket_addr().ip() = msg.role_attr().socket_addr().ip();
  rtpsMsg->changemsg().role_attr().socket_addr().port() = msg.role_attr().socket_addr().port();
  rtpsMsg->changemsg().role_attr().service_name() = msg.role_attr().service_name();
  rtpsMsg->changemsg().role_attr().service_id() = msg.role_attr().service_id();

}

void Manager::Convert(const RoleAttributes& attr, RoleType role,
                      OperateType opt, ChangeMsg* msg) {
  msg->set_timestamp(cyber::Time::Now().ToNanosecond());
  msg->set_change_type(change_type_);
  msg->set_operate_type(opt);
  msg->set_role_type(role);
  auto role_attr = msg->mutable_role_attr();
  role_attr->CopyFrom(attr);
  if (!role_attr->has_host_name()) {
    role_attr->set_host_name(host_name_);
  }
  if (!role_attr->has_process_id()) {
    role_attr->set_process_id(process_id_);
  }
#ifdef CYBER_DBG
//TO test if it's possilbe to avoid '\0' protobuf serialize data.No way
  msg->set_reserved("reserved");
#endif
}

void Manager::Notify(const ChangeMsg& msg) { signal_(msg); }

//void Manager::OnRemoteChange(const std::string& msg_str) {
void Manager::OnRemoteChange(const cyber::transport::UnderlayMessage& msg_str) {

#ifdef CYBER_DBG
  AINFO << "Manager::OnRemoteChange Start,msg:";
#endif

  if (is_shutdown_.load()) {
    ADEBUG << "the manager has been shut down.";
    return;
  }

  ChangeMsg msg;

#ifdef CYBER_BUGFIX
  //fastdds issue:
  //https://github.com/eProsima/Fast-DDS/issues/2689#issuecomment-1132705252
 RtpsMsgConvert(msg_str,&msg);
  
#else
  RETURN_IF(!message::ParseFromString(msg_str, &msg));

#endif


#ifdef CYBER_DBG

  AINFO << "Manager::OnRemoteChange"<<
  "\n msg change_type:"<< msg.change_type() << 
  "\n msg operate_type:" << msg.operate_type() <<
  "\n msg role type:" << msg.role_type() <<
  "\nmsg role attr hot ip:" << msg.role_attr().host_ip() <<
  "\nmsg role attr node name:" << msg.role_attr().node_name() <<
  "\nmsg role attr channel_name:" << msg.role_attr().channel_name();

#endif
  if (IsFromSameProcess(msg)) {
    return;
  }
  RETURN_IF(!Check(msg.role_attr()));
  Dispose(msg);
}

bool Manager::Publish(const ChangeMsg& msg) {
  if (!is_discovery_started_.load()) {
    ADEBUG << "discovery is not started.";
    return false;
  }
#ifdef CYBER_DBG
  AINFO << "Manager::Publish Start";
#endif
  apollo::cyber::transport::UnderlayMessage m;

#ifdef CYBER_BUGFIX
  //fastdds issue:
  //https://github.com/eProsima/Fast-DDS/issues/2689#issuecomment-1132705252
  ProtoMsgConvert(msg,&m);
#else
  RETURN_VAL_IF(!message::SerializeToString(msg, &m.data()), false);
#endif
  {

    std::lock_guard<std::mutex> lg(lock_);
    if (publisher_ != nullptr) {
#ifdef CYBER_DBG
  //AINFO <<"serializer data:\n";
  //log_hexdump("DBG",16,(uint8_t*)m.data().c_str(),m.data().size());

#endif
     return publisher_->write(reinterpret_cast<void*>(&m));
    }
  }
  return true;
}

bool Manager::IsFromSameProcess(const ChangeMsg& msg) {
  auto& host_name = msg.role_attr().host_name();
  int process_id = msg.role_attr().process_id();

  if (process_id != process_id_ || host_name != host_name_) {
    return false;
  }
  return true;
}

}  // namespace service_discovery
}  // namespace cyber
}  // namespace apollo
