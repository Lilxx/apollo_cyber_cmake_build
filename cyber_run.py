from argparse import ArgumentParser
from multiprocessing import cpu_count
import os
import subprocess

def run_cmd(_cmd):
    """
    开启子进程，执行对应指令，控制台打印执行过程，然后返回子进程执行的状态码和执行返回的数据
    :param _cmd: 子进程命令
    :return: 子进程状态码和执行结果
    """
    p = subprocess.Popen(_cmd, shell=True, close_fds=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    _RunCmdStdout, _ColorStdout = [], '\033[1;35m{0}\033[0m'
    while p.poll() is None:
        line = p.stdout.readline().rstrip()
        if not line:
            continue
        _RunCmdStdout.append(line)
        #print(_ColorStdout.format(line))
    last_line = p.stdout.read().rstrip()
    if last_line:
        _RunCmdStdout.append(last_line)
        #print(_ColorStdout.format(last_line))
    _RunCmdReturn = p.wait()
    return _RunCmdReturn, b'\n'.join(_RunCmdStdout), p.stderr.read()


def elf_file_check(file):
    code, stdout, stderr = run_cmd("file "+file)
    #get ELF file and skip so
    if("ELF 64-bit" in stdout.decode() and ".so" not in file):
        return True
    else:
        return False

all_file = []

def get_program(path):
    files = os.listdir(path)
    for fi in files:
        fi_d = os.path.join(path,fi)            
        if os.path.isdir(fi_d):
            get_program(fi_d)                  
        else:  
            if(elf_file_check(fi_d)):
                all_file.append(os.path.join(path,fi_d))
     
 

def setup_cyber_env(enable_debug_log=False):
    pwd = os.getcwd()+"/dependency/lib"
    os.environ['LD_LIBRARY_PATH']=pwd
    os.environ['CYBER_DOMAIN_ID']="80"
    os.environ['CYBER_IP']="127.0.0.1"

    os.system("mkdir -p /tmp/cyber/data/log")
    os.environ['GLOG_log_dir']="/tmp/cyber/data/log"
    os.environ['GLOG_alsologtostderr']="1"
    os.environ['GLOG_colorlogtostderr']="1"
    os.environ['GLOG_minlogl']="0"
    if enable_debug_log:
        os.environ['GLOG_v']="4"
    #change to work dir
    os.chdir(os.getcwd()+"/install")


def run(program_name):
    os.system(program_name)


def parse_option():
    # 构造ArgumentParser
    parser = ArgumentParser(usage='start cyber framwork or examples or cyber tools',
                            description='set option from cmdline',
                            prefix_chars='-')
    # print(parser.prog, parser.usage, parser.description)
    # 定义命令行参数

    parser.add_argument('-s', '--show',action='store_true',help="show all program to run",dest='show')
    parser.add_argument('-r', '--run',action='store_true',help="choice certain program to run",dest='run')
 
    return parser.parse_args()

def show(enable):
    prc_dict = {}

    if(enable):
        for i in range(len(all_file)):
            prc_dict[i] = all_file[i]
            print("index:%d,program name%s"%(i,all_file[i]))
    return prc_dict

if __name__ == "__main__":
    options = parse_option()
    #print(options.number, options.fun)
    get_program(os.getcwd()+"/install")
    prc_dict = {}
    if(options.show):
        show(True)
        exit(1)
    if(options.run):
        prc_dict = show(True)
        index = input("Please input the index to run desired program or input e to exit:")
        if(index == "e"):
            exit(1)
        if(int(index)<0 or int(index) > len(all_file)-1):
            #exception
            print("Error Index,Exit")
            exit(1)
        setup_cyber_env()
        cmd = input("Please input the cmd args to pass to  desired program:")
        os.system(prc_dict[int(index)]+" "+cmd)
    #run("./bin/cyber -h")