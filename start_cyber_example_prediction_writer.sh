#!/bin/bash



export LD_LIBRARY_PATH="$(pwd)"/dependency/lib
#must change work dir to here
cd "$(pwd)"/install
export CYBER_PATH="$(pwd)"
export CYBER_DOMAIN_ID=80
export CYBER_IP=127.0.0.1

mkdir -p /tmp/cyber/data/log
export GLOG_log_dir="/tmp/cyber/data/log"
export GLOG_alsologtostderr=1
export GLOG_colorlogtostderr=1
export GLOG_minloglevel=0
#export GLOG_v=4 #Enable Debug Log

export sysmo_start=0
#start example

./bin/channel_prediction_writer 
