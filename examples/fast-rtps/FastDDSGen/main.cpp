
#include "fastcdr/Cdr.h"
#include "ChangeMsg.h"
#include "ChangeMsgPubSubTypes.h"


using namespace eprosima::fastcdr;

#define BUFER_SIZE 4096

int main()
{
    ChangeMsg msg;
    ChangeMsg msg_de;

    msg.timestamp() = 12345;
    msg.change_type() = CHANGE_CHANNEL;
    msg.role_type() = ROLE_NODE;
    msg.role_attr().host_name() = "ubuntu";
    msg.role_attr().host_ip() = "127.0.0.1";
    msg.role_attr().process_id() = 6666;
    msg.role_attr().node_name() = "test";
    msg.role_attr().channel_name() = "test channel";
    msg.role_attr().socket_addr().port() = 11111;

    SerializedPayload_t payload;
    char buffer[BUFER_SIZE] = {0};
    FastBuffer fastbuffer(buffer,BUFER_SIZE); // Object that manages the raw buffer.
    Cdr ser(fastbuffer,eprosima::fastcdr::Cdr::DEFAULT_ENDIAN,
            eprosima::fastcdr::Cdr::DDS_CDR); 

    msg.serialize(ser);

    FastBuffer fastbuffer1(buffer,BUFER_SIZE); // Object that manages the raw buffer.
    Cdr der(fastbuffer1,eprosima::fastcdr::Cdr::DEFAULT_ENDIAN,
            eprosima::fastcdr::Cdr::DDS_CDR); 

    msg_de.deserialize(der);

    std::cout << msg_de.timestamp()<<std::endl;

    return 0;
}