

#cd ./build
#rm -rf *
#cd ..
cmake  -B build -G Ninja 

 
startTime=`date +%Y%m%d-%H:%M:%S`
startTime_s=`date +%s`
cd ./build

ninja install


#-------------------------------
endTime=`date +%Y%m%d-%H:%M:%S`
endTime_s=`date +%s`
 
sumTime=$[ $endTime_s - $startTime_s ]
 
echo "$startTime ---> $endTime" "Total:$sumTime seconds"

#generate build relationship
#ninja -t graph all |dot -T png -o graph.png